# Nextcloud Dark

A dark theme for the web interface of Nextcloud. Current status: WIP.

This style is written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), although it might be compatible with other style managers such as [xStyle](https://github.com/FirefoxBar/xStyle).

## Install with Stylus

If you have Stylus installed, click on the banner below and a new window of Stylus should open, asking you to confirm to install the style.

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/nextcloud-dark/raw/master/nextcloud-dark.user.css)

## Install manually or with other style managers

In the case of other style managers, you may need to copy and paste the [raw CSS code](https://gitlab.com/maxigaz/nextcloud-dark/raw/master/nextcloud-dark.user.css) in the settings of such addons.

## Applied domains

Currently, the style changes the looks of the web interface of the Nextcloud instance hosted at disroot.org. Feel free to edit the domain so that it matches your own personal instance.

If you want me to add another public instance, let me know (create an issue).
